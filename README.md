# roomba-navigator

This is a repository about ROS1 code for controlling an iRobot Create 2 with an Xbox One controller, to practice robotics related applications such as SLAM (Simultaneous Localization And Mapping) and path planning.

# Prerequisites
Install the software below before running `catkin_make` for this repository.
+ **[libcreate](https://github.com/popeyedpanther/libcreate)**

# Install
Start by creating a new catkin workspace using the commands below. 
```bash
mkdir -p ~/roomba_ws/src
cd roomba_ws/src
git clone --recursive https://gitlab.com/roomba-bot/roomba-navigator.git .
```
The `--recursive` is to download all the dependent packages such as [scanse-driver](https://github.com/scanse/sweep-ros) in this repository.
The `.` in the end of the command line is important, since it stops creating another folder when you use `git clone` command.

After downloading everything, make the package:
```bash
cd ..
catkin_make
```
# Collaboration Rule
To collaborate in this repository, please follow instructions below:
+ Create your local branch:
    ```bash
    cd roomba_ws/src
    git checkout -b local_branch
    ```
    Where `local_branch` is the name of any branch other than `master`.
+ Make changes: `git add` and `git commit` 
    + Please **comment** in each commit to let others know what contributions you made. This **[commit style](http://udacity.github.io/git-styleguide/)** is strongly recommended.
+ Push to the repository:
    ```bash
    git push origin local_branch
    ```
+ Then create a [merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html) to master branch





